import 'dart:async';
import 'dart:math';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  double boxPosX = 0;
  double boxPosY = 0;
  double boxSpeedX = 0;
  double boxSpeedY = 0;
  double boxSize = 180;
  double movementSpeed = 7;
  double screenBorderW = 0;
  double screenBorderH = 0;
  String imageURL = "https://picsum.photos/180/180?random=1";

  void changePosition(Timer t) async {
    setState(() {
      boxPosX += boxSpeedX;
      boxPosY += boxSpeedY;
      // RIGHT
      if (boxPosX + boxSize >= screenBorderW) {
        boxSpeedX = -movementSpeed;
      }
      // LEFT
      if (boxPosX <= 0) {
        boxSpeedX = movementSpeed;
      }
      // UP
      if (boxPosY <= 0) {
        boxSpeedY = movementSpeed;
      }
      // DOWN
      if (boxPosY + boxSize >= screenBorderH) {
        boxSpeedY = -movementSpeed;
      }
    });
  }

  @override
  void initState() {
    super.initState();
    boxPosX = boxSize;
    boxPosY = boxSize;
    Timer.periodic(const Duration(milliseconds: 16), changePosition);
  }

  @override
  Widget build(BuildContext context) {
    if (screenBorderW == 0) screenBorderW = MediaQuery.of(context).size.width;
    if (screenBorderH == 0) screenBorderH = MediaQuery.of(context).size.height;

    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Material(color: Colors.yellowAccent),
          Positioned(
            // duration: Duration(seconds: 1),
            left: boxPosX,
            top: boxPosY,
            child: GestureDetector(
              onHorizontalDragUpdate: (details) {
                if (details.delta.dx > 0) {
                  boxSpeedX = movementSpeed;
                } else if (details.delta.dx < 0) {
                  boxSpeedX = -movementSpeed;
                }
              },
              onVerticalDragUpdate: (details) {
                if (details.delta.dy > 0) {
                  boxSpeedY = movementSpeed;
                } else if (details.delta.dy < 0) {
                  boxSpeedY = -movementSpeed;
                }
              },
              onTap: () {
                resolveRandomURL(
                    "https://picsum.photos/${boxSize.toInt()}/${boxSize.toInt()}?random=${Random().nextInt(1000)}");
              },
              child: Container(
                width: boxSize,
                height: boxSize,
                decoration: BoxDecoration(
                  color: Colors.red,
                  image: DecorationImage(
                      image: NetworkImage(imageURL), fit: BoxFit.cover),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void resolveRandomURL(String url) async {
    final client = new http.Client();
    final request = http.Request('GET', Uri.parse(url))
      ..followRedirects = false;
    final response = await client.send(request);
    print(url);
    print(response.headers['location']);
    print(response.statusCode);
    if (response.statusCode == 302) {
      imageURL = response.headers['location'].toString();
      print(imageURL);
    }
    // return response.headers['location'].toString();
  }
}
